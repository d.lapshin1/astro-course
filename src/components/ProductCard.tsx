import { IProduct } from "../types"
import { useState } from 'preact/hooks'

interface ProductCardProps {
    product: IProduct
}


export default function ProductCard({ product }: ProductCardProps) {
    const [details, setDetails] = useState(false)
    const toggle = () => setDetails((prev) => !prev)

    return <div className="border py-2 px-4 rounded flex flex-col items-center mb-2">
        <h3 className='mb-2'>{product.title}</h3>
        <a href={`/product/${product.id}`}>Open</a>
        <img src={product.image} className='w-1/4' alt={product.title} />
        <span className='font-bold'>{product.price}</span>
        <button className="py-2 px-4 border bg-yellow-400" onClick={toggle}>Toggle</button>
        {
            details && <div>
                <p>{product.description}</p>
                <p>Rate: <span style={{ fontWeight: 'bold' }}>{product?.rating?.rate}</span> </p>
            </div>
        }

    </div>
}